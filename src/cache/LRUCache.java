package cache;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Andrei Dziubiankou
 *
 * @param <K> type for key
 * @param <V> type for value
 */
public class LRUCache<K, V> implements Cache<K, V> {
    private final LinkedHashMap<K, V> map;
    private final ReadWriteLock rwLock;
    private final Lock rLock;
    private final Lock wLock;
    /**
     * @param maxSize size of cashe
     */
    @SuppressWarnings("serial")
    public LRUCache(final int maxSize) {
        super();
        map = new LinkedHashMap<K, V>(maxSize, (float) 0.75, true) {
            private final int maxEntries = maxSize;
            protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
                return size() > maxEntries;
            }
        };
        rwLock = new ReentrantReadWriteLock();
        rLock = rwLock.readLock();
        wLock = rwLock.writeLock();
    }

    @Override
    public void put(final K key, final V value) {
        // TODO Auto-generated method stub
        wLock.lock();
        try {
            map.put(key, value);
            System.out.println(this);
            System.out.println(this.size());
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } finally {
            wLock.unlock();
        }
    }

    @Override
    public V get(final K key) {
        // TODO Auto-generated method stub
        rLock.lock();       //if don"t need synchronized reading, delete try-finally and lock()-unlock()
        try {
            if (map.containsKey(key)) {
               return map.get(key);
            } else {
                return null;
            }
        } finally {
            rLock.unlock();
        }
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return map.toString();
    }

}
