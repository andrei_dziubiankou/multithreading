package cache;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Andrei Dziubiankou
 *
 * @param <K> type for key
 * @param <V> type for value
 */
public class LFUCache<K, V> implements Cache<K, V> {
    private final LinkedHashMap<K, CacheEntry<V>> map;
    private final int maxEntries;
    private final ReadWriteLock rwLock;
    private final Lock rLock;
    private final Lock wLock;

    /**
     * @param maxSize size of cashe
     */

    private final class CacheEntry<T> {
        private T data;
        private int frequency;

        CacheEntry() { }

        public T getData() {
            return data;
        }
        public void setData(final T data) {
            this.data = data;
        }

        public int getFrequency() {
            return frequency;
        }
        public void setFrequency(final int frequency) {
            this.frequency = frequency;
        }

        @Override
        public String toString() {
            // TODO Auto-generated method stub
            return " frequency=" + getFrequency() + ";value=" + getData();
        }
    }

    /**
     * @param maxSize of cache
     */
    public LFUCache(final int maxSize) {
        // TODO Auto-generated constructor stub
        maxEntries = maxSize;
        map = new LinkedHashMap<K, CacheEntry<V>>(maxSize);
        rwLock = new ReentrantReadWriteLock();
        rLock = rwLock.readLock();
        wLock = rwLock.writeLock();
    }

    @Override
    public void put(final K key, final V value) {
        // TODO Auto-generated method stub
        wLock.lock();
        try {
            if (isFull()) {
                K entryKeyToBeRemoved = getLFURemovedKey();
                map.remove(entryKeyToBeRemoved);
            }
            CacheEntry<V> temp = new CacheEntry<V>();
            temp.setData(value);
            temp.setFrequency(0);
            map.put(key, temp);
            System.out.println(this);
            System.out.println(this.size());
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } finally {
            wLock.unlock();
        }
    }

    @Override
    public V get(final K key) {
        // TODO Auto-generated method stub
        rLock.lock();       //if don"t need synchronized reading, delete try-finally and lock()-unlock()
        try {
            if (map.containsKey(key)) {
                CacheEntry<V> temp = map.get(key);
                int frequency = temp.getFrequency();
                frequency++;
                temp.setFrequency(frequency);
                return temp.getData();
            } else {
                return null;
            }
        } finally {
            rLock.unlock();
        }
    }

    private boolean isFull() {
        if (size() == maxEntries) {
            return true;
        }
        return false;
    }

    private K getLFURemovedKey() {
        K key = null;
        int minFreq = Integer.MAX_VALUE;

        for (Map.Entry<K, CacheEntry<V>> entry : map.entrySet()) {
            if (minFreq > entry.getValue().frequency) {
                key = entry.getKey();
                minFreq = entry.getValue().frequency;
            }
        }

        return key;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return map.size();
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return map.toString();
    }

}
