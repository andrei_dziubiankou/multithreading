package cache;

/**
 * @author Andrei Dziubiankou
 * @param <K>
 * @param <V>
 */
public interface Cache<K, V> {
    /**
     * @param key for object
     * @param value of object (reference)
     */
    void put(K key, V value);
    /**
     * @param key for object
     * @return object reference
     */
    V get(K key);
    /**
     * @return current size of cache
     */
    int size();
}
