import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cache.Cache;
import cache.LFUCache;
import cache.LRUCache;

/**
 * @author Andrei Dziubiankou
 *
 */
public final class Runner {

    private Runner() {
        //don't to be used
    }

    /**
     * @param args for console
     */
    public static void main(final String[] args) {
        System.out.println("demonstration");
        String[] keys1 = {"A", "B", "C"};
        String[] keys2 = {"D", "E", "F"};
        Integer[] values1 = {5, 7, 9};
        Integer[] values2 = {11, 13, 15};
        Cache<String, Integer> cache = new LRUCache<String, Integer>(5); //change to LFUCache for demonstare LFUCache

        Runnable threadOne = new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                System.out.println("threadOne start");
                for (int i = 0; i < keys1.length; i++) {
                    cache.put(keys1[i], values1[i]);
                    /*if (i == 2) {
                        cache.get(keys1[0]);     //for test
                    }*/
                }
            }
        };

        Runnable threadTwo = new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                System.out.println("threadTwo start");
                for (int i = 0; i < keys2.length; i++) {
                    cache.put(keys2[i], values2[i]);
                }
            }
        };

        ExecutorService es = Executors.newFixedThreadPool(2);
        es.execute(threadOne);
        es.execute(threadTwo);
        es.shutdown();
    }

}
